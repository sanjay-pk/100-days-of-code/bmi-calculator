const button = document.getElementById("calculateBtn");
const displayBmi = document.getElementById("bmiResult");
const result = document.getElementById("result");
const bmiCondition = document.getElementById("bmiCondition")

button.addEventListener("click", calculateBmi);

function calculateBmi(event) {
    event.preventDefault();

    // Get input values and selected gender
    const inputWeight = parseFloat(document.getElementById("weight").value);
    const inputHeight = parseFloat(document.getElementById("height").value);
    const gender = document.querySelector('input[name="gender"]:checked');

    // Check if all required inputs are valid
    if (isNaN(inputWeight) || isNaN(inputHeight) || !gender) {
        alert("Please fill out all fields correctly.");
        return;
    }

    // Convert height from centimeters (cm) to meters (m)
    const heightM = inputHeight / 100;

    // Calculate BMI
    const bmi = inputWeight / (heightM * heightM);

    // Show the result display
    result.style.display = "block";

    // // Display BMI result in the designated element
    // displayBmi.textContent = bmi.toFixed(2);

    document.getElementById("weight").value = ""; // Clear weight input
    document.getElementById("height").value = ""; // Clear height input
    document.getElementById("age").value = "";    // Clear age input

    bmiResult(bmi);
}

function bmiResult(bmi){
    let bmiCategory = '';

    // Determine BMI category based on BMI value
    if (bmi < 18.5) {
        bmiCategory = 'Underweight';
    } else if (bmi >= 18.5 && bmi < 24.9) {
        bmiCategory = 'Normal weight';
    } else if (bmi >= 25 && bmi < 29.9) {
        bmiCategory = 'Overweight';
    } else {
        bmiCategory = 'Obese';
    }
// display Bmi and category
    const bmiCat = `BMI: ${bmi.toFixed(2)} (${bmiCategory})`;
    displayBmi.textContent  = bmiCat
}